import sys
import os
import urllib3

class FlyResource:

    def __init__(self, concourse_url, concourse_user, concourse_pass, branch, pipeline_file_path):
        self.concourse_user = concourse_user
        self.concourse_pass = concourse_pass
        self.pipeline_file_path = pipeline_file_path
        self.branch = branch
        self.concourse_url = concourse_url
        if "win" in sys.platform:
            self.fly_download_link = concourse_url + "/api/v1/cli?arch=amd64&platform=windows"
            self.fly_file = "fly_command.exe"
        else:
            self.fly_download_link = concourse_url + "/api/v1/cli?arch=amd64&platform=linux"
            self.fly_file = "fly_command"
        print(self.fly_file)

    def push_pipeline(self):
        self.__download_fly_cli()
        self.__login_to_concourse()
        self.__create_pipeline()
        self.__unpause_pipeline()
        pass

    def __download_fly_cli(self):
        print("downloading fly cli ...")
        http = urllib3.PoolManager()
        r = http.request('GET', self.fly_download_link, preload_content=False)
        with open(self.fly_file, 'wb') as out:
            while True:
                data = r.read(2024)
                if not data:
                    break
                out.write(data)
        r.release_conn()
        print("fly downloaded")
        os.system(self.fly_file + " -v")
        pass

    def __login_to_concourse(self):
        command = "{} -t ci login -c {} -u {} -p {}".format(self.fly_file, self.concourse_url, self.concourse_user, self.concourse_pass)
        print(command)
        os.system(command)
        pass

    def __create_pipeline(self):
        command = "{} -t ci set-pipeline -p {} -c {} -n".format(self.fly_file, self.branch, self.pipeline_file_path)
        print(command)
        os.system(command)
        pass

    def __unpause_pipeline(self):
        command = "{} -t ci unpause-pipeline -p {}".format(self.fly_file, self.branch)
        print(command)
        os.system(command)
        pass

    def clean_up(self):
        os.remove(self.fly_file)
