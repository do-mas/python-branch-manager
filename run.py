import sys

from pipe_file_builder import PipelineFileBuilder
from fly_resource import FlyResource

def execute_pipeline_creation(concourse_url = sys.argv[1],
                              concourse_user = sys.argv[2],
                              concourse_pass = sys.argv[3],
                              pivotal_pass = sys.argv[4],
                              branch=sys.argv[5],
                              git_repo=sys.argv[6]):

    pipeline_file_manager = PipelineFileBuilder(branch, git_repo, pivotal_pass)
    pipeline_config = pipeline_file_manager.create_pipeline_config()

    fly_command = FlyResource(concourse_url, concourse_user, concourse_pass, branch, pipeline_config)
    fly_command.push_pipeline()

    fly_command.clean_up()
    pipeline_file_manager.clean_up()

if __name__ == "__main__":
    execute_pipeline_creation()
