from string import Template
import os
import jinja2
import shutil


class PipelineFileBuilder:

    def __init__(self, branch, git_repo, pivotal_pass):
        os.system("mkdir pipelines")
        self.branch = branch
        self.git_repo = git_repo
        self.pivotal_pass = pivotal_pass
        self.pipeline_file_path = self.__get_pipeline_file_name()
        pass

    def create_pipeline_config(self):
        print("creating pipeline config")
        os.system("mkdir pipelines")

        pipeline_content = self.__get_pipeline_content(self.branch, self.git_repo, self.pivotal_pass)
        self.__create_pipeline_file(pipeline_content, self.pipeline_file_path)

        return self.pipeline_file_path

    def __get_pipeline_content(self, branch, git_repo, pivotal_pass):
        context = {'branch': branch, 'git_repo': git_repo, 'pivotal_pass':pivotal_pass}
        result = self.__render('pipeline-template.yml', context)
        return result

    def __render(self, template_path, context):
        path, filename = os.path.split(template_path)
        return jinja2.Environment(loader=jinja2.FileSystemLoader(path or './')).get_template(filename).render(context)

    def __get_pipeline_file_name(self):
        pipeline_file_path_template = Template('pipelines/pipeline-${branch}.yml')
        pipeline_file_path = pipeline_file_path_template.safe_substitute(branch=self.branch)
        return pipeline_file_path

    def __create_pipeline_file(self, pipeline_content, pipeline_file_path):
        pipeline_file = open(pipeline_file_path, 'a')
        pipeline_file.write(pipeline_content)
        pipeline_file.close()

    def clean_up(self):
        shutil.rmtree('pipelines')
